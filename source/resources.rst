Tools & Resources
=================

A collection of tools and resources are available for those designing for the GNOME platform.

General Resources
-----------------

* Color palette: `Inkscape/GIMP format <https://gitlab.gnome.org/Teams/Design/HIG-app-icons/raw/master/GNOME%20HIG.gpl?inline=false>`_, :doc:`reference table <reference/palette>`
* Cantarell font: pre-installed in GNOME-based distributions and `available to download <https://cantarell.gnome.org>`_.

Apps
----

The following apps are all available to install:

* `Icon Library <https://flathub.org/apps/details/org.gnome.design.IconLibrary>`_: for finding icons to use in GNOME UI
* `Typography <https://flathub.org/apps/details/org.gnome.design.Typography>`_: for selecting text styles and commonly used characters
* `App Icon Preview <https://flathub.org/apps/details/org.gnome.design.AppIconPreview>`_: app icon creation assistant
* `Symbolic Preview <https://flathub.org/apps/details/org.gnome.design.SymbolicPreview>`_: symbolic icon creation assistant
* `Color Palette <https://flathub.org/apps/details/org.gnome.design.Palette>`_: reference for the GNOME color palette

Toolkit
-------

The following tools are useful for seeing how the different patterns and design elements work in practice:

* Adwaita Demo: a demo app for libadwaita. This can be installed from the `gnome-nightly flatpak repository <https://nightly.gnome.org/repo/appstream/org.gnome.Adwaita1.Demo.flatpakref>`_ and is sometimes packaged by distributions as ``adwaita-1-demo``.
* `GTK inspector <https://docs.gtk.org/gtk4/running.html#interactive-debugging>`_: can be used to inspect any GTK app

SVG Templates & Examples
------------------------

The following templates can be used as a starting point for mockups and icons.

* `App mockup template (SVG) <https://gitlab.gnome.org/Teams/Design/mockup-resources>`_
* `App icon template (SVG) <https://gitlab.gnome.org/Teams/Design/HIG-app-icons/-/blob/master/template.svg>`_

Additionally, since GNOME design operates in the open, `all its work  is therefore publicly available, and can be freely reused <https://gitlab.gnome.org/Teams/Design>`_. (However, be aware that not all of this work is up to date or follows the latest recommendations.)

